<?php

/**
 * Implementation of hook_rules_action_info().
 */
function cleverreach_rules_action_info() {
  $rules = array();
  $rules['cleverreach_subscribe'] = array(
    'label' => t('Subscribe mailaddress to Cleverreach'),
    'module' => 'cleverreach',
    'group' => t('Cleverreach'),
    'parameter' => array(
      'mailaddress' => array(
        'type' => 'text',
        'label' => t('Mailadress to subscribe'),
        'description' => t('Add the mailaddress token you want to subscribe.'),
      ),
      'list' => array(
        'type' => 'text',
        'label' => t('Cleverreach list'),
        'description' => t('Choose the Cleverreach list you want the mailaddress to subscribe to.'),
        'options list' => 'cleverreach_lists',
        'restriction' => 'input',
        'allow null' => TRUE,
        'optional' => TRUE,
        'default value' => NULL,
      ),
    ),
  );

  $rules['cleverreach_unsubscribe'] = array(
    'label' => t('Unsubscribe mailaddress from Cleverreach'),
    'module' => 'cleverreach',
    'group' => t('Cleverreach'),
    'parameter' => array(
      'mailaddress' => array(
        'type' => 'text',
        'label' => t('Mailadress to unsubscribe'),
        'description' => t('Add the mailaddress token you want to unsubscribe.'),
      ),
      'list' => array(
        'type' => 'text',
        'label' => t('Cleverreach list'),
        'description' => t('Choose the Cleverreach list you want the mailaddress to unsubscribe from.'),
        'options list' => 'cleverreach_lists',
        'restriction' => 'input',
        'allow null' => TRUE,
        'optional' => TRUE,
        'default value' => NULL,
      ),
    ),
  );

  return $rules;
}

/**
 * Get synchronized Cleverreach lists.
 */

function cleverreach_lists() {
  $crgroups = cleverreach_get_all_groups();
  $groups = array();

  foreach($crgroups as $crgroup) {
    $groups[$crgroup->id] = $crgroup->name;
  }
  $groups[NULL] = '';

  return $groups;
}


/**
 * Implements rules action "cleverreach_subscribe".
 */

function cleverreach_subscribe($mailaddress, $list) {
  $params = array(
    "email" => $mailaddress,
    "registered" => time(),
    "activated" => time(),
    "source" => variable_get('site_name', 'CleverReach Drupal Module'),
  );

  if($list == NULL) {
    drupal_set_message(t('Please select a Cleverreach Newsletter List in your rules configuration.'), 'error');
  } else {
    $settings = _cleverreach_get_settings();
    $api = new SoapClient($settings["url"]);
    $result = $api->receiverAdd($settings["key"], $list, $params);
  }
}

/**
 * Implements rules action "cleverreach_subscribe".
 */

function cleverreach_unsubscribe($mailaddress, $list) {

  if($list == NULL) {
    drupal_set_message(t('Please select a Cleverreach Newsletter List in your rules configuration.'), 'error');
  } else {
    $settings = _cleverreach_get_settings();
    $api = new SoapClient($settings["url"]);
    $result = $api->receiverDelete($settings["key"], $list, $mailaddress);
  }
}
